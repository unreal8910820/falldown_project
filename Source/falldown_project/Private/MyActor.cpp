// Fill out your copyright notice in the Description page of Project Settings.


#include "MyActor.h"
#include "Engine/StaticMesh.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
AMyActor::AMyActor()
{
	static ConstructorHelpers::FObjectFinder<UStaticMesh> SM_SPHERE(
	TEXT("/Script/Engine.StaticMesh'/Engine/EngineMeshes/Sphere.Sphere'"));

 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ConstructorHelpers::FObjectFinder<UMaterial> M_COIN(
		TEXT("/Script/Engine.Material'/Game/Assets/Materials/M_Coin.M_Coin'"));

	// SphereComponent 생성
	CoinArea = CreateDefaultSubobject<USphereComponent>(TEXT("COIN_AREA"));
	// CreateDefaultSubobject<T>() : 객체를 생성하는 멤버 함수
	// 생성자에서 사용할 수 있습니다.

	// StaticMeshComponent 생성
	CoinMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("COIN_MESH"));

	// 루트 컴포넌트를 지정합니다.
	//RootComponent = CoinArea;
	SetRootComponent(CoinArea);
	
	// CoinMesh 컴포넌트를 CoinArea의 하위 컴포넌트로 지정합니다.
	CoinMesh->SetupAttachment(CoinArea);

	// CoinMesh 컴포넌트의 상대적 크기를 설정합니다.
	CoinMesh->SetRelativeScale3D(FVector(0.25f, 1.0f, 1.0f));

	if (SM_SPHERE.Succeeded())
	{
		CoinMesh->SetStaticMesh(SM_SPHERE.Object);
	}

	if (M_COIN.Succeeded())
	{
		CoinMesh->SetMaterial(0, M_COIN.Object);
	}

	RotationSpeed = FRotator(0.0f, 10.0f, 0.0f);
}

// Called when the game starts or when spawned
void AMyActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// 현재 회전을 얻습니다.
	FRotator currentRotation = CoinMesh->GetRelativeRotation();

	currentRotation += RotationSpeed;

}

